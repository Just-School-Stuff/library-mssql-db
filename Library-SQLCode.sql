create database [LibraryDB];

use [LibraryDB];

create table dbo.Library (
	Lib_ID varchar(50) not null,
	Lib_Name nvarchar(50) not null,
	Address_ID varchar(50) not null unique,
	constraint PK_Library primary key (Lib_ID)
);

create table dbo.Employee (
	Emp_ID varchar(50) not null,
	Emp_TCID varchar(11) not null,
	Emp_Name nchar(10) not null,
	Emp_SurName nchar(10) not null,
	Emp_PhoneNo varchar(10) not null,
	Emp_Task nvarchar(15) null,
	Emp_Birth date null,
	Emp_Entry date not null,
	Emp_Dissmissal date null,
	Emp_Footnote nvarchar(15) null,
	Lib_ID varchar(50) not null,
	constraint PK_Employee primary key (Emp_ID, Emp_TCID)
);

create table dbo.Member (
	Mem_ID varchar(50) not null,
	Mem_TCID varchar(11) not null,
	Mem_Name nvarchar(15) not null,
	Mem_SurName nvarchar(15) not null,
	Mem_PhoneNo varchar(10) not null,
	Mem_Entry date not null,
	Mem_Dissmissal date null,
	Mem_Footnote nvarchar(15) null,
	Lib_ID varchar(50) not null,
	constraint PK_Member primary key (Mem_ID, Mem_TCID)
);

create table dbo.[Address] (
	Address_ID varchar(50) not null,
	Street nvarchar(50) not null,
	Neighborhood nvarchar(50) not null,
	BuildingName nvarchar(15) not null,
	BuildingNo int not null,
	District nvarchar(50) not null,
	Country nvarchar(50) not null,
	PostalCode int not null,
	constraint PK_Address primary key (Address_ID)
);

create table dbo.Escrow (
	Esc_ID varchar(10) not null,
	Esc_Receiving date not null,
	Esc_Back date not null,
	isBack bit not null,
	Book_ID nvarchar(50) not null,
	Mem_ID varchar(50) not null,
	Mem_TCID varchar(11) not null,
	Lib_ID varchar(50) not null,
	constraint PK_Escrow primary key (Esc_ID, Lib_ID)
);

create table dbo.Publisher (
	Pub_ID varchar(10) not null,
	Pub_Name nvarchar(20) not null,
	constraint PK_Publisher primary key (Pub_ID)
);

create table dbo.Author (
	Aut_ID varchar(10) not null,
	Aut_Name nvarchar(20) not null,
	Aut_Surname nvarchar(20) not null,
	constraint PK_Author primary key (Aut_ID)
);

create table dbo.Category (
	Cat_ID int not null,
	Cat_Name nvarchar(15) not null,
	Cat_Exp nvarchar(50) null,
	constraint PK_Category primary key (Cat_ID)
);

create table dbo.Book (
	Book_ID nvarchar(50) not null,
	Book_Name nvarchar(50) not null,
	Book_Cat nchar(10) null,
	Book_Exp nvarchar(50) null,
	Book_Donor nvarchar(50) null,
	Book_Receiving date not null,
	BSh_ID varchar(50) not null,
	Aut_ID varchar(10) not null,
	Pub_ID varchar(10) not null,
	Lib_ID varchar(50) not null,
	constraint PK_Book primary key (Book_ID, Lib_ID)
);

create table dbo.Category_Book (
	Book_ID nvarchar(50) not null,
	Lib_ID varchar(50) not null,
	Cat_ID int not null
)

create table dbo.Bookshelf (
	BSh_ID varchar(50) not null,
	BSh_Exp nvarchar(50) null,
	Cat_ID int not null,
	BCs_ID varchar(50) not null,
	Lib_ID varchar(50) not null,
	constraint PK_Bookshelf primary key (BSh_ID)
);

create table dbo.Bookcase (
	BCs_ID varchar(50) not null,
	BCs_Loc nvarchar(50) not null,
	BCs_Exp nvarchar(50) not null,
	Lib_ID varchar(50) not null,
	constraint PK_Bookcase primary key (BCs_ID)
);

/*
	// Data Input

	insert dbo.Library (
	Address_ID,
	Street, 
	Neighborhood, 
	BuildingName, 
	BuildingNo, 
	District, 
	Country, 
	PostalCode
) 
values (
	N'Address_01', 
	N'street_01', 
	N'neighborhood_01',
	N'buildingName_01',
	N'0001',
	N'district_01',
	N'country_01',
	321
)

	insert dbo.Address (Address_ID, Lib_Name, Address_ID) values (N'Lib_01', N'Newyork Lib', N'Address_01')	
	
	
*/

alter table dbo.Library with check add constraint FK_Library_Address foreign key (Address_ID)
references dbo.[Address] (Address_ID);

alter table dbo.Employee with check add constraint FK_Employee_Library foreign key (Lib_ID)
references dbo.Library (Lib_ID);

alter table dbo.Member with check add constraint FK_Member_Library foreign key (Lib_ID)
references dbo.Library (Lib_ID);

alter table dbo.Escrow with check add constraint FK_Escrow_Library foreign key (Lib_ID)
references dbo.Library (Lib_ID);

alter table dbo.Book with check add constraint FK_Book_Library foreign key (Lib_ID)
references dbo.Library (Lib_ID);

alter table dbo.Bookshelf with check add constraint FK_Bookshelf_Library foreign key (Lib_ID)
references dbo.Library (Lib_ID);

alter table dbo.Bookcase with check add constraint FK_Bookcase_Library foreign key (Lib_ID)
references dbo.Library (Lib_ID);

alter table dbo.Escrow with check add constraint FK_Escrow_Member foreign key (Mem_ID, Mem_TCID)
references dbo.Member (Mem_ID, Mem_TCID);

alter table dbo.Escrow with check add constraint FK_Escrow_Book foreign key (Book_ID)
references dbo.Book (Book_ID);

alter table dbo.Book with check add constraint FK_Book_Author foreign key (Aut_ID)
references dbo.Author (Aut_ID);

alter table dbo.Book with check add constraint FK_Book_Publisher foreign key (Pub_ID)
references dbo.Publisher (Pub_ID);

alter table dbo.Book with check add constraint FK_Book_Bookshelf foreign key (BSh_ID)
references dbo.Bookshelf (BSh_ID);

alter table dbo.Category_Book with check add constraint FK_Category_Book_Book foreign key (Book_ID)
references dbo.Book (Book_ID);

alter table dbo.Category_Book with check add constraint FK_Category_Book_Category foreign key (Cat_ID)
references dbo.Category (Cat_ID);

alter table dbo.Bookshelf with check add constraint FK_Bookshelf_Category foreign key (Cat_ID)
references dbo.Category (Cat_ID);

alter table dbo.Bookshelf with check add constraint FK_Bookshelf_Bookcase foreign key (BCs_ID)
references dbo.Bookcase (BCs_ID);
