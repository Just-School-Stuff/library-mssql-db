USE [master]
GO
/****** Object:  Database [kutuphane]    Script Date: 20.10.2018 16:16:21 ******/
CREATE DATABASE [kutuphane]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'kutuphane', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSBSQLSERVER\MSSQL\DATA\kutuphane.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 10%)
 LOG ON 
( NAME = N'kutuphane_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSBSQLSERVER\MSSQL\DATA\kutuphane_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [kutuphane] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [kutuphane].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [kutuphane] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [kutuphane] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [kutuphane] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [kutuphane] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [kutuphane] SET ARITHABORT OFF 
GO
ALTER DATABASE [kutuphane] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [kutuphane] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [kutuphane] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [kutuphane] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [kutuphane] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [kutuphane] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [kutuphane] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [kutuphane] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [kutuphane] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [kutuphane] SET  DISABLE_BROKER 
GO
ALTER DATABASE [kutuphane] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [kutuphane] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [kutuphane] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [kutuphane] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [kutuphane] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [kutuphane] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [kutuphane] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [kutuphane] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [kutuphane] SET  MULTI_USER 
GO
ALTER DATABASE [kutuphane] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [kutuphane] SET DB_CHAINING OFF 
GO
ALTER DATABASE [kutuphane] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [kutuphane] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [kutuphane] SET DELAYED_DURABILITY = DISABLED 
GO
USE [kutuphane]
GO
/****** Object:  Table [dbo].[Adres]    Script Date: 20.10.2018 16:16:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Adres](
	[Adres_ID] [varchar](50) NOT NULL,
	[Cadde] [nvarchar](50) NULL,
	[Sokak] [nvarchar](50) NULL,
	[Mahalle] [nvarchar](50) NULL,
	[Bina] [nchar](10) NULL,
	[BinaNo] [int] NULL,
	[Il] [nvarchar](50) NULL,
	[Ilce] [nvarchar](50) NULL,
	[PostaKodu] [int] NULL,
 CONSTRAINT [PK_Adres] PRIMARY KEY CLUSTERED 
(
	[Adres_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Emanet]    Script Date: 20.10.2018 16:16:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Emanet](
	[E_ID] [nchar](10) NULL,
	[E_ATarihi] [date] NOT NULL,
	[E_VTarihi] [date] NOT NULL,
	[K_ID] [nvarchar](10) NOT NULL,
	[U_ID] [nvarchar](10) NOT NULL,
	[U_TC] [varchar](11) NOT NULL,
	[Kut_ID] [varchar](50) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Gorevli]    Script Date: 20.10.2018 16:16:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Gorevli](
	[G_ID] [nvarchar](10) NOT NULL,
	[G_TC] [varchar](11) NOT NULL,
	[G_Isim] [nchar](10) NULL,
	[G_SoyIsim] [nchar](10) NULL,
	[G_TelNo] [varchar](10) NULL,
	[G_Gorev] [nvarchar](15) NULL,
	[G_DogumTarihi] [date] NULL,
	[G_GirisTarihi] [date] NULL,
	[G_CikisTarihi] [date] NULL,
	[G_Durumu] [nvarchar](15) NULL,
	[G_Aciklama] [nvarchar](50) NULL,
	[Kut_ID] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Gorevli] PRIMARY KEY CLUSTERED 
(
	[G_ID] ASC,
	[G_TC] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Kategori]    Script Date: 20.10.2018 16:16:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Kategori](
	[KT_Isim] [nvarchar](10) NOT NULL,
	[KT_Aciklama] [nvarchar](50) NULL,
 CONSTRAINT [PK_Kategori] PRIMARY KEY CLUSTERED 
(
	[KT_Isim] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Kategori_Kitap]    Script Date: 20.10.2018 16:16:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Kategori_Kitap](
	[K_ID] [nvarchar](10) NOT NULL,
	[KT_Isim] [nvarchar](10) NOT NULL,
 CONSTRAINT [PK_Kategori_Kitap] PRIMARY KEY CLUSTERED 
(
	[KT_Isim] ASC,
	[K_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Kitap]    Script Date: 20.10.2018 16:16:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Kitap](
	[K_ID] [nvarchar](10) NOT NULL,
	[K_Isim] [nvarchar](50) NOT NULL,
	[K_Kategori] [nchar](10) NULL,
	[K_Aciklama] [nvarchar](50) NULL,
	[K_Bagislayan] [nvarchar](50) NULL,
	[K_GirisTarihi] [date] NULL,
	[Raf_ID] [varchar](10) NOT NULL,
	[Y_ID] [varchar](10) NOT NULL,
	[YE_ID] [varchar](10) NULL,
	[Kut_ID] [varchar](50) NOT NULL CONSTRAINT [DF_Kitap_Kut_ID]  DEFAULT ('K01'),
 CONSTRAINT [PK_Kitap] PRIMARY KEY CLUSTERED 
(
	[K_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Kitaplik]    Script Date: 20.10.2018 16:16:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Kitaplik](
	[Kit_ID] [varchar](5) NOT NULL,
	[Kit_Konum] [nvarchar](50) NULL,
	[Kit_Aciklama] [nvarchar](50) NULL,
	[Kut_ID] [varchar](50) NOT NULL CONSTRAINT [DF_Kitaplik_Kut_ID]  DEFAULT ('K01'),
 CONSTRAINT [PK_Kitaplik] PRIMARY KEY CLUSTERED 
(
	[Kit_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Kutuphane]    Script Date: 20.10.2018 16:16:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Kutuphane](
	[Kut_ID] [varchar](50) NOT NULL,
	[Kut_Ismi] [nvarchar](50) NULL,
	[Adres_ID] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Kutuphane] PRIMARY KEY CLUSTERED 
(
	[Kut_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Raf]    Script Date: 20.10.2018 16:16:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Raf](
	[Raf_ID] [varchar](10) NOT NULL,
	[Raf_Aciklama] [nvarchar](50) NULL,
	[KT_Isim] [nvarchar](10) NOT NULL,
	[Kit_ID] [varchar](5) NOT NULL,
	[Kut_ID] [varchar](50) NOT NULL CONSTRAINT [DF_Raf_Kut_ID]  DEFAULT ('K01'),
 CONSTRAINT [PK_Raf] PRIMARY KEY CLUSTERED 
(
	[Raf_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Uye]    Script Date: 20.10.2018 16:16:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Uye](
	[U_ID] [nvarchar](10) NOT NULL,
	[U_TC] [varchar](11) NOT NULL,
	[U_Isim] [nchar](10) NULL,
	[U_SoyIsim] [nchar](10) NULL,
	[U_TelNo] [varchar](10) NULL,
	[U_GirisTarihi] [date] NULL,
	[U_CikisTarihi] [date] NULL,
	[U_Durumu] [nvarchar](15) NULL,
	[U_Aciklama] [nvarchar](50) NULL,
	[U_DogumTarihi] [date] NULL,
	[Kut_ID] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Uye] PRIMARY KEY CLUSTERED 
(
	[U_ID] ASC,
	[U_TC] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[YayinEvi]    Script Date: 20.10.2018 16:16:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[YayinEvi](
	[YE_ID] [varchar](10) NOT NULL,
	[YE_Isim] [nvarchar](15) NULL,
 CONSTRAINT [PK_YayinEvi] PRIMARY KEY CLUSTERED 
(
	[YE_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Yazar]    Script Date: 20.10.2018 16:16:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Yazar](
	[Y_ID] [varchar](10) NOT NULL,
	[Y_Isim] [nvarchar](20) NULL,
	[Y_Soyisim] [nvarchar](20) NULL,
 CONSTRAINT [PK_Yazar] PRIMARY KEY CLUSTERED 
(
	[Y_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[Adres] ([Adres_ID], [Cadde], [Sokak], [Mahalle], [Bina], [BinaNo], [Il], [Ilce], [PostaKodu]) VALUES (N'A06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Adres] ([Adres_ID], [Cadde], [Sokak], [Mahalle], [Bina], [BinaNo], [Il], [Ilce], [PostaKodu]) VALUES (N'I34', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Kategori] ([KT_Isim], [KT_Aciklama]) VALUES (N'Bilim', N'Bilim Temalı Kategori')
INSERT [dbo].[Kategori] ([KT_Isim], [KT_Aciklama]) VALUES (N'Korku', N'Korku Temalı Kategori')
INSERT [dbo].[Kategori] ([KT_Isim], [KT_Aciklama]) VALUES (N'Mat', N'Matematik Temalı Kategori')
INSERT [dbo].[Kategori_Kitap] ([K_ID], [KT_Isim]) VALUES (N'003', N'Bilim')
INSERT [dbo].[Kategori_Kitap] ([K_ID], [KT_Isim]) VALUES (N'004', N'Bilim')
INSERT [dbo].[Kategori_Kitap] ([K_ID], [KT_Isim]) VALUES (N'001', N'Korku')
INSERT [dbo].[Kategori_Kitap] ([K_ID], [KT_Isim]) VALUES (N'002', N'Korku')
INSERT [dbo].[Kategori_Kitap] ([K_ID], [KT_Isim]) VALUES (N'003', N'Mat')
INSERT [dbo].[Kategori_Kitap] ([K_ID], [KT_Isim]) VALUES (N'011', N'Mat')
INSERT [dbo].[Kitap] ([K_ID], [K_Isim], [K_Kategori], [K_Aciklama], [K_Bagislayan], [K_GirisTarihi], [Raf_ID], [Y_ID], [YE_ID], [Kut_ID]) VALUES (N'001', N'Katil1', N'Korku     ', N'A-Korku', NULL, NULL, N'S1K1R1', N'Y01', NULL, N'K01')
INSERT [dbo].[Kitap] ([K_ID], [K_Isim], [K_Kategori], [K_Aciklama], [K_Bagislayan], [K_GirisTarihi], [Raf_ID], [Y_ID], [YE_ID], [Kut_ID]) VALUES (N'002', N'Katil2', N'Korku     ', N'A-Korku', NULL, NULL, N'S1K1R1', N'Y01', NULL, N'K01')
INSERT [dbo].[Kitap] ([K_ID], [K_Isim], [K_Kategori], [K_Aciklama], [K_Bagislayan], [K_GirisTarihi], [Raf_ID], [Y_ID], [YE_ID], [Kut_ID]) VALUES (N'003', N'Katil3', N'Korku     ', N'A-Korku', NULL, NULL, N'S1K1R2', N'Y01', NULL, N'K01')
INSERT [dbo].[Kitap] ([K_ID], [K_Isim], [K_Kategori], [K_Aciklama], [K_Bagislayan], [K_GirisTarihi], [Raf_ID], [Y_ID], [YE_ID], [Kut_ID]) VALUES (N'004', N'Katil4', N'Korku     ', N'A-Korku', NULL, NULL, N'S1K1R2', N'Y01', NULL, N'K01')
INSERT [dbo].[Kitap] ([K_ID], [K_Isim], [K_Kategori], [K_Aciklama], [K_Bagislayan], [K_GirisTarihi], [Raf_ID], [Y_ID], [YE_ID], [Kut_ID]) VALUES (N'011', N'Dünyanın Oluşumu', N'Bilim     ', N'A-Bilm', NULL, NULL, N'S1K2R1', N'Y03', NULL, N'K01')
INSERT [dbo].[Kitap] ([K_ID], [K_Isim], [K_Kategori], [K_Aciklama], [K_Bagislayan], [K_GirisTarihi], [Raf_ID], [Y_ID], [YE_ID], [Kut_ID]) VALUES (N'012', N'Dünyanın Oluşumu', N'Bilim     ', N'A-Bilm', NULL, NULL, N'S1K2R1', N'Y03', NULL, N'K01')
INSERT [dbo].[Kitap] ([K_ID], [K_Isim], [K_Kategori], [K_Aciklama], [K_Bagislayan], [K_GirisTarihi], [Raf_ID], [Y_ID], [YE_ID], [Kut_ID]) VALUES (N'013', N'Dünyanın Oluşumu', N'Bilim     ', N'A-Bilm', NULL, NULL, N'S1K2R2', N'Y03', NULL, N'K01')
INSERT [dbo].[Kitap] ([K_ID], [K_Isim], [K_Kategori], [K_Aciklama], [K_Bagislayan], [K_GirisTarihi], [Raf_ID], [Y_ID], [YE_ID], [Kut_ID]) VALUES (N'015', N'Dünyanın Oluşumu', N'Bilim     ', N'A-Bilm', NULL, NULL, N'S1K2R2', N'Y03', NULL, N'K01')
INSERT [dbo].[Kitap] ([K_ID], [K_Isim], [K_Kategori], [K_Aciklama], [K_Bagislayan], [K_GirisTarihi], [Raf_ID], [Y_ID], [YE_ID], [Kut_ID]) VALUES (N'016', N'Matematiğe Giriş', N'Mat       ', N'A-Mat', NULL, NULL, N'S1K2R4', N'Y02', NULL, N'K01')
INSERT [dbo].[Kitap] ([K_ID], [K_Isim], [K_Kategori], [K_Aciklama], [K_Bagislayan], [K_GirisTarihi], [Raf_ID], [Y_ID], [YE_ID], [Kut_ID]) VALUES (N'017', N'Matematiğe Giriş2', N'Mat       ', N'A-Mat', NULL, NULL, N'S1K2R4', N'Y02', NULL, N'K01')
INSERT [dbo].[Kitaplik] ([Kit_ID], [Kit_Konum], [Kit_Aciklama], [Kut_ID]) VALUES (N'S1K1', N'Soldan 1. Sıra 1. Kitaplık', N'Korku', N'K01')
INSERT [dbo].[Kitaplik] ([Kit_ID], [Kit_Konum], [Kit_Aciklama], [Kut_ID]) VALUES (N'S1K2', N'Soldan 1. Sıra 1. Kitaplık', N'Bilim-Mat', N'K01')
INSERT [dbo].[Kitaplik] ([Kit_ID], [Kit_Konum], [Kit_Aciklama], [Kut_ID]) VALUES (N'S2K1', N'Soldan 1. Sıra 3. Kitaplık', N'Bilim', N'K01')
INSERT [dbo].[Kitaplik] ([Kit_ID], [Kit_Konum], [Kit_Aciklama], [Kut_ID]) VALUES (N'S2K2', N'Soldan 2. Sıra 2. Kitaplık', N'Bilim-Mat', N'K01')
INSERT [dbo].[Kitaplik] ([Kit_ID], [Kit_Konum], [Kit_Aciklama], [Kut_ID]) VALUES (N'S3K1', N'Soldan 3. Sıra 1. Kitaplk', N'Mat', N'K01')
INSERT [dbo].[Kutuphane] ([Kut_ID], [Kut_Ismi], [Adres_ID]) VALUES (N'K01', N'Alaçatı Kütüphanesi', N'I34')
INSERT [dbo].[Kutuphane] ([Kut_ID], [Kut_Ismi], [Adres_ID]) VALUES (N'K02', N'Büyükçekmece Kütüphanesi', N'A06')
INSERT [dbo].[Raf] ([Raf_ID], [Raf_Aciklama], [KT_Isim], [Kit_ID], [Kut_ID]) VALUES (N'S1K1R1', NULL, N'Korku', N'S1K1', N'K01')
INSERT [dbo].[Raf] ([Raf_ID], [Raf_Aciklama], [KT_Isim], [Kit_ID], [Kut_ID]) VALUES (N'S1K1R2', NULL, N'Korku', N'S1K1', N'K01')
INSERT [dbo].[Raf] ([Raf_ID], [Raf_Aciklama], [KT_Isim], [Kit_ID], [Kut_ID]) VALUES (N'S1K1R3', NULL, N'Korku', N'S1K1', N'K01')
INSERT [dbo].[Raf] ([Raf_ID], [Raf_Aciklama], [KT_Isim], [Kit_ID], [Kut_ID]) VALUES (N'S1K1R4', NULL, N'Korku', N'S1K1', N'K01')
INSERT [dbo].[Raf] ([Raf_ID], [Raf_Aciklama], [KT_Isim], [Kit_ID], [Kut_ID]) VALUES (N'S1K2R1', NULL, N'Bilim', N'S1K2', N'K01')
INSERT [dbo].[Raf] ([Raf_ID], [Raf_Aciklama], [KT_Isim], [Kit_ID], [Kut_ID]) VALUES (N'S1K2R2', NULL, N'Bilim', N'S1K2', N'K01')
INSERT [dbo].[Raf] ([Raf_ID], [Raf_Aciklama], [KT_Isim], [Kit_ID], [Kut_ID]) VALUES (N'S1K2R3', NULL, N'Bilim', N'S1K2', N'K01')
INSERT [dbo].[Raf] ([Raf_ID], [Raf_Aciklama], [KT_Isim], [Kit_ID], [Kut_ID]) VALUES (N'S1K2R4', NULL, N'Mat', N'S1K2', N'K01')
INSERT [dbo].[Raf] ([Raf_ID], [Raf_Aciklama], [KT_Isim], [Kit_ID], [Kut_ID]) VALUES (N'S2K1R1', NULL, N'Bilim', N'S2K1', N'K01')
INSERT [dbo].[Raf] ([Raf_ID], [Raf_Aciklama], [KT_Isim], [Kit_ID], [Kut_ID]) VALUES (N'S2K1R2', NULL, N'Bilim', N'S2K1', N'K01')
INSERT [dbo].[Raf] ([Raf_ID], [Raf_Aciklama], [KT_Isim], [Kit_ID], [Kut_ID]) VALUES (N'S2K1R3', NULL, N'Bilim', N'S2K1', N'K01')
INSERT [dbo].[Raf] ([Raf_ID], [Raf_Aciklama], [KT_Isim], [Kit_ID], [Kut_ID]) VALUES (N'S2K1R4', NULL, N'Bilim', N'S2K1', N'K01')
INSERT [dbo].[Raf] ([Raf_ID], [Raf_Aciklama], [KT_Isim], [Kit_ID], [Kut_ID]) VALUES (N'S2K2R1', NULL, N'Mat', N'S2K2', N'K01')
INSERT [dbo].[Raf] ([Raf_ID], [Raf_Aciklama], [KT_Isim], [Kit_ID], [Kut_ID]) VALUES (N'S2K2R2', NULL, N'Bilim', N'S2K2', N'K01')
INSERT [dbo].[Raf] ([Raf_ID], [Raf_Aciklama], [KT_Isim], [Kit_ID], [Kut_ID]) VALUES (N'S2K2R3', NULL, N'Bilim', N'S2K2', N'K01')
INSERT [dbo].[Raf] ([Raf_ID], [Raf_Aciklama], [KT_Isim], [Kit_ID], [Kut_ID]) VALUES (N'S2K2R4', NULL, N'Mat', N'S2K2', N'K01')
INSERT [dbo].[Raf] ([Raf_ID], [Raf_Aciklama], [KT_Isim], [Kit_ID], [Kut_ID]) VALUES (N'S3K1R1', NULL, N'Mat', N'S3K1', N'K01')
INSERT [dbo].[Raf] ([Raf_ID], [Raf_Aciklama], [KT_Isim], [Kit_ID], [Kut_ID]) VALUES (N'S3K1R2', NULL, N'Mat', N'S3K1', N'K01')
INSERT [dbo].[Raf] ([Raf_ID], [Raf_Aciklama], [KT_Isim], [Kit_ID], [Kut_ID]) VALUES (N'S3K1R3', NULL, N'Mat', N'S3K1', N'K01')
INSERT [dbo].[Raf] ([Raf_ID], [Raf_Aciklama], [KT_Isim], [Kit_ID], [Kut_ID]) VALUES (N'S3K1R4', NULL, N'Mat', N'S3K1', N'K01')
INSERT [dbo].[Yazar] ([Y_ID], [Y_Isim], [Y_Soyisim]) VALUES (N'Y01', N'Ali', N'Soydan')
INSERT [dbo].[Yazar] ([Y_ID], [Y_Isim], [Y_Soyisim]) VALUES (N'Y02', N'Berk', N'Saman')
INSERT [dbo].[Yazar] ([Y_ID], [Y_Isim], [Y_Soyisim]) VALUES (N'Y03', N'Sinem', N'Balken')
ALTER TABLE [dbo].[Emanet] ADD  CONSTRAINT [DF_Emanet_Kut_ID]  DEFAULT ('K01') FOR [Kut_ID]
GO
ALTER TABLE [dbo].[Gorevli] ADD  CONSTRAINT [DF_Gorevli_Kut_ID]  DEFAULT ('Kut1') FOR [Kut_ID]
GO
ALTER TABLE [dbo].[Emanet]  WITH CHECK ADD  CONSTRAINT [FK_Emanet_Kitap] FOREIGN KEY([K_ID])
REFERENCES [dbo].[Kitap] ([K_ID])
GO
ALTER TABLE [dbo].[Emanet] CHECK CONSTRAINT [FK_Emanet_Kitap]
GO
ALTER TABLE [dbo].[Emanet]  WITH CHECK ADD  CONSTRAINT [FK_Emanet_Kutuphane] FOREIGN KEY([Kut_ID])
REFERENCES [dbo].[Kutuphane] ([Kut_ID])
GO
ALTER TABLE [dbo].[Emanet] CHECK CONSTRAINT [FK_Emanet_Kutuphane]
GO
ALTER TABLE [dbo].[Emanet]  WITH CHECK ADD  CONSTRAINT [FK_Emanet_Uye] FOREIGN KEY([U_ID], [U_TC])
REFERENCES [dbo].[Uye] ([U_ID], [U_TC])
GO
ALTER TABLE [dbo].[Emanet] CHECK CONSTRAINT [FK_Emanet_Uye]
GO
ALTER TABLE [dbo].[Gorevli]  WITH CHECK ADD  CONSTRAINT [FK_Gorevli_Kutuphane] FOREIGN KEY([Kut_ID])
REFERENCES [dbo].[Kutuphane] ([Kut_ID])
GO
ALTER TABLE [dbo].[Gorevli] CHECK CONSTRAINT [FK_Gorevli_Kutuphane]
GO
ALTER TABLE [dbo].[Kategori_Kitap]  WITH CHECK ADD  CONSTRAINT [FK_Kategori_Kitap_Kategori] FOREIGN KEY([KT_Isim])
REFERENCES [dbo].[Kategori] ([KT_Isim])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Kategori_Kitap] CHECK CONSTRAINT [FK_Kategori_Kitap_Kategori]
GO
ALTER TABLE [dbo].[Kategori_Kitap]  WITH CHECK ADD  CONSTRAINT [FK_Kategori_Kitap_Kitap] FOREIGN KEY([K_ID])
REFERENCES [dbo].[Kitap] ([K_ID])
GO
ALTER TABLE [dbo].[Kategori_Kitap] CHECK CONSTRAINT [FK_Kategori_Kitap_Kitap]
GO
ALTER TABLE [dbo].[Kitap]  WITH CHECK ADD  CONSTRAINT [FK_Kitap_Kutuphane] FOREIGN KEY([Kut_ID])
REFERENCES [dbo].[Kutuphane] ([Kut_ID])
GO
ALTER TABLE [dbo].[Kitap] CHECK CONSTRAINT [FK_Kitap_Kutuphane]
GO
ALTER TABLE [dbo].[Kitap]  WITH CHECK ADD  CONSTRAINT [FK_Kitap_Raf] FOREIGN KEY([Raf_ID])
REFERENCES [dbo].[Raf] ([Raf_ID])
GO
ALTER TABLE [dbo].[Kitap] CHECK CONSTRAINT [FK_Kitap_Raf]
GO
ALTER TABLE [dbo].[Kitap]  WITH CHECK ADD  CONSTRAINT [FK_Kitap_YayinEvi] FOREIGN KEY([YE_ID])
REFERENCES [dbo].[YayinEvi] ([YE_ID])
GO
ALTER TABLE [dbo].[Kitap] CHECK CONSTRAINT [FK_Kitap_YayinEvi]
GO
ALTER TABLE [dbo].[Kitap]  WITH CHECK ADD  CONSTRAINT [FK_Kitap_Yazar] FOREIGN KEY([Y_ID])
REFERENCES [dbo].[Yazar] ([Y_ID])
GO
ALTER TABLE [dbo].[Kitap] CHECK CONSTRAINT [FK_Kitap_Yazar]
GO
ALTER TABLE [dbo].[Kitaplik]  WITH CHECK ADD  CONSTRAINT [FK_Kitaplik_Kutuphane] FOREIGN KEY([Kut_ID])
REFERENCES [dbo].[Kutuphane] ([Kut_ID])
GO
ALTER TABLE [dbo].[Kitaplik] CHECK CONSTRAINT [FK_Kitaplik_Kutuphane]
GO
ALTER TABLE [dbo].[Kutuphane]  WITH CHECK ADD  CONSTRAINT [FK_Kutuphane_Adres] FOREIGN KEY([Adres_ID])
REFERENCES [dbo].[Adres] ([Adres_ID])
GO
ALTER TABLE [dbo].[Kutuphane] CHECK CONSTRAINT [FK_Kutuphane_Adres]
GO
ALTER TABLE [dbo].[Raf]  WITH CHECK ADD  CONSTRAINT [FK_Raf_Kategori] FOREIGN KEY([KT_Isim])
REFERENCES [dbo].[Kategori] ([KT_Isim])
GO
ALTER TABLE [dbo].[Raf] CHECK CONSTRAINT [FK_Raf_Kategori]
GO
ALTER TABLE [dbo].[Raf]  WITH CHECK ADD  CONSTRAINT [FK_Raf_Kitaplik] FOREIGN KEY([Kit_ID])
REFERENCES [dbo].[Kitaplik] ([Kit_ID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Raf] CHECK CONSTRAINT [FK_Raf_Kitaplik]
GO
ALTER TABLE [dbo].[Raf]  WITH CHECK ADD  CONSTRAINT [FK_Raf_Kutuphane] FOREIGN KEY([Kut_ID])
REFERENCES [dbo].[Kutuphane] ([Kut_ID])
GO
ALTER TABLE [dbo].[Raf] CHECK CONSTRAINT [FK_Raf_Kutuphane]
GO
ALTER TABLE [dbo].[Uye]  WITH CHECK ADD  CONSTRAINT [FK_Uye_Kutuphane] FOREIGN KEY([Kut_ID])
REFERENCES [dbo].[Kutuphane] ([Kut_ID])
GO
ALTER TABLE [dbo].[Uye] CHECK CONSTRAINT [FK_Uye_Kutuphane]
GO
USE [master]
GO
ALTER DATABASE [kutuphane] SET  READ_WRITE 
GO
