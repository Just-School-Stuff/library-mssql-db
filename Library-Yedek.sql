USE [master]
GO
/****** Object:  Database [LibraryDB]    Script Date: 7.11.2018 22:05:27 ******/
CREATE DATABASE [LibraryDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'LibraryDB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSBSQLSERVER\MSSQL\DATA\LibraryDB.mdf' , SIZE = 4288KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'LibraryDB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSBSQLSERVER\MSSQL\DATA\LibraryDB_log.ldf' , SIZE = 1072KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [LibraryDB] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [LibraryDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [LibraryDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [LibraryDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [LibraryDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [LibraryDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [LibraryDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [LibraryDB] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [LibraryDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [LibraryDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [LibraryDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [LibraryDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [LibraryDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [LibraryDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [LibraryDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [LibraryDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [LibraryDB] SET  ENABLE_BROKER 
GO
ALTER DATABASE [LibraryDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [LibraryDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [LibraryDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [LibraryDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [LibraryDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [LibraryDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [LibraryDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [LibraryDB] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [LibraryDB] SET  MULTI_USER 
GO
ALTER DATABASE [LibraryDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [LibraryDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [LibraryDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [LibraryDB] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [LibraryDB] SET DELAYED_DURABILITY = DISABLED 
GO
/* For security reasons the login is created disabled and with a random password. */
/****** Object:  Login [msb]    Script Date: 7.11.2018 22:05:27 ******/
CREATE LOGIN [msb] WITH PASSWORD=N'fQ^O°`GÖ¿:¹DøN,èM¬$¾{re¤Î³f;', DEFAULT_DATABASE=[master], DEFAULT_LANGUAGE=[Türkçe], CHECK_EXPIRATION=ON, CHECK_POLICY=ON
GO
ALTER LOGIN [msb] DISABLE
GO
ALTER AUTHORIZATION ON DATABASE::[LibraryDB] TO [msb]
GO
ALTER SERVER ROLE [processadmin] ADD MEMBER [msb]
GO
ALTER SERVER ROLE [diskadmin] ADD MEMBER [msb]
GO
ALTER SERVER ROLE [dbcreator] ADD MEMBER [msb]
GO
ALTER SERVER ROLE [bulkadmin] ADD MEMBER [msb]
GO
USE [LibraryDB]
GO
/****** Object:  Table [dbo].[Address]    Script Date: 7.11.2018 22:05:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Address](
	[Address_ID] [varchar](50) NOT NULL,
	[Street] [nvarchar](50) NOT NULL,
	[Neighborhood] [nvarchar](50) NOT NULL,
	[BuildingName] [nvarchar](15) NOT NULL,
	[BuildingNo] [int] NOT NULL,
	[District] [nvarchar](50) NOT NULL,
	[Country] [nvarchar](50) NOT NULL,
	[PostalCode] [int] NOT NULL,
 CONSTRAINT [PK_Address] PRIMARY KEY CLUSTERED 
(
	[Address_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER AUTHORIZATION ON [dbo].[Address] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[Author]    Script Date: 7.11.2018 22:05:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Author](
	[Aut_ID] [varchar](10) NOT NULL,
	[Aut_Name] [nvarchar](20) NOT NULL,
	[Aut_Surname] [nvarchar](20) NOT NULL,
 CONSTRAINT [PK_Author] PRIMARY KEY CLUSTERED 
(
	[Aut_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER AUTHORIZATION ON [dbo].[Author] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[Book]    Script Date: 7.11.2018 22:05:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Book](
	[Book_ID] [nvarchar](50) NOT NULL,
	[Book_Name] [nvarchar](50) NOT NULL,
	[Book_Exp] [nvarchar](50) NULL,
	[Book_Donor] [nvarchar](50) NULL,
	[Book_Receiving] [date] NOT NULL,
	[BCs_ID] [varchar](50) NOT NULL,
	[BSh_ID] [varchar](50) NOT NULL,
	[Aut_ID] [varchar](10) NOT NULL,
	[Pub_ID] [varchar](10) NOT NULL,
	[Lib_ID] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Book] PRIMARY KEY CLUSTERED 
(
	[Book_ID] ASC,
	[Lib_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER AUTHORIZATION ON [dbo].[Book] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[Bookcase]    Script Date: 7.11.2018 22:05:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Bookcase](
	[BCs_ID] [varchar](50) NOT NULL,
	[BCs_Loc] [nvarchar](50) NULL,
	[BCs_Exp] [nvarchar](50) NULL,
	[Lib_ID] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Bookcase] PRIMARY KEY CLUSTERED 
(
	[BCs_ID] ASC,
	[Lib_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER AUTHORIZATION ON [dbo].[Bookcase] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[Bookshelf]    Script Date: 7.11.2018 22:05:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Bookshelf](
	[BSh_ID] [varchar](50) NOT NULL,
	[BSh_Exp] [nvarchar](50) NULL,
	[Cat_ID] [int] NOT NULL,
	[BCs_ID] [varchar](50) NOT NULL,
	[Lib_ID] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Bookshelf] PRIMARY KEY CLUSTERED 
(
	[BSh_ID] ASC,
	[BCs_ID] ASC,
	[Lib_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER AUTHORIZATION ON [dbo].[Bookshelf] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[Category]    Script Date: 7.11.2018 22:05:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[Cat_ID] [int] NOT NULL,
	[Cat_Name] [nvarchar](15) NOT NULL,
	[Cat_Exp] [nvarchar](50) NULL,
 CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED 
(
	[Cat_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[Category] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[Category_Book]    Script Date: 7.11.2018 22:05:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Category_Book](
	[Book_ID] [nvarchar](50) NOT NULL,
	[Lib_ID] [varchar](50) NOT NULL,
	[Cat_ID] [int] NOT NULL,
 CONSTRAINT [PK_Category_Book] PRIMARY KEY CLUSTERED 
(
	[Book_ID] ASC,
	[Cat_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER AUTHORIZATION ON [dbo].[Category_Book] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[Employee]    Script Date: 7.11.2018 22:05:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Employee](
	[Emp_ID] [varchar](50) NOT NULL,
	[Emp_TCID] [varchar](11) NOT NULL,
	[Emp_Name] [nchar](10) NOT NULL,
	[Emp_SurName] [nchar](10) NOT NULL,
	[Emp_PhoneNo] [varchar](10) NOT NULL,
	[Emp_Salary] [int] NOT NULL,
	[Emp_Task] [nvarchar](15) NULL,
	[Emp_Birth] [date] NULL,
	[Emp_Entry] [date] NOT NULL,
	[Emp_Dissmissal] [date] NULL,
	[Emp_Footnote] [nvarchar](15) NULL,
	[Lib_ID] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Employee] PRIMARY KEY CLUSTERED 
(
	[Emp_ID] ASC,
	[Emp_TCID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER AUTHORIZATION ON [dbo].[Employee] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[Escrow]    Script Date: 7.11.2018 22:05:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Escrow](
	[Esc_ID] [varchar](10) NOT NULL,
	[Esc_Receiving] [date] NOT NULL,
	[Esc_Back] [date] NOT NULL,
	[isBack] [bit] NOT NULL,
	[Book_ID] [nvarchar](50) NOT NULL,
	[Mem_ID] [varchar](50) NOT NULL,
	[Mem_TCID] [varchar](11) NOT NULL,
	[Lib_ID] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Escrow] PRIMARY KEY CLUSTERED 
(
	[Esc_ID] ASC,
	[Lib_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER AUTHORIZATION ON [dbo].[Escrow] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[Library]    Script Date: 7.11.2018 22:05:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Library](
	[Lib_ID] [varchar](50) NOT NULL,
	[Lib_Name] [nvarchar](50) NOT NULL,
	[Address_ID] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Library] PRIMARY KEY CLUSTERED 
(
	[Lib_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER AUTHORIZATION ON [dbo].[Library] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[Member]    Script Date: 7.11.2018 22:05:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Member](
	[Mem_ID] [varchar](50) NOT NULL,
	[Mem_TCID] [varchar](11) NOT NULL,
	[Mem_Name] [nvarchar](15) NOT NULL,
	[Mem_SurName] [nvarchar](15) NOT NULL,
	[Mem_PhoneNo] [varchar](10) NOT NULL,
	[Mem_Entry] [date] NOT NULL,
	[Mem_Dissmissal] [date] NULL,
	[Mem_Footnote] [nvarchar](15) NULL,
	[Lib_ID] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Member] PRIMARY KEY CLUSTERED 
(
	[Mem_TCID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER AUTHORIZATION ON [dbo].[Member] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[Publisher]    Script Date: 7.11.2018 22:05:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Publisher](
	[Pub_ID] [varchar](10) NOT NULL,
	[Pub_Name] [nvarchar](20) NOT NULL,
 CONSTRAINT [PK_Publisher] PRIMARY KEY CLUSTERED 
(
	[Pub_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER AUTHORIZATION ON [dbo].[Publisher] TO  SCHEMA OWNER 
GO
INSERT [dbo].[Address] ([Address_ID], [Street], [Neighborhood], [BuildingName], [BuildingNo], [District], [Country], [PostalCode]) VALUES (N'A0001', N'Street1', N'NB1', N'BuildingName1', 21, N'Esenyurt', N'İstanbul', 345625)
INSERT [dbo].[Address] ([Address_ID], [Street], [Neighborhood], [BuildingName], [BuildingNo], [District], [Country], [PostalCode]) VALUES (N'A0002', N'Street2', N'NB2', N'BuildingName2', 32, N'Esenyurt', N'İstanbul', 345625)
INSERT [dbo].[Author] ([Aut_ID], [Aut_Name], [Aut_Surname]) VALUES (N'AUT0001', N'Fatih', N'Samancı')
INSERT [dbo].[Book] ([Book_ID], [Book_Name], [Book_Exp], [Book_Donor], [Book_Receiving], [BCs_ID], [BSh_ID], [Aut_ID], [Pub_ID], [Lib_ID]) VALUES (N'B00001', N'Gök Taşları', NULL, NULL, CAST(N'2018-05-03' AS Date), N'BCS0001', N'BSH0001', N'AUT0001', N'PUB0001', N'K0001')
INSERT [dbo].[Book] ([Book_ID], [Book_Name], [Book_Exp], [Book_Donor], [Book_Receiving], [BCs_ID], [BSh_ID], [Aut_ID], [Pub_ID], [Lib_ID]) VALUES (N'B00002', N'Gök Taşlarının Matematiği', NULL, NULL, CAST(N'2018-05-03' AS Date), N'BCS0002', N'BSH0002', N'AUT0001', N'PUB0001', N'K0001')
INSERT [dbo].[Book] ([Book_ID], [Book_Name], [Book_Exp], [Book_Donor], [Book_Receiving], [BCs_ID], [BSh_ID], [Aut_ID], [Pub_ID], [Lib_ID]) VALUES (N'B00003', N'Kitap1.3', NULL, NULL, CAST(N'2013-05-06' AS Date), N'BCS0001', N'BSH0001', N'AUT0001', N'PUB0001', N'K0001')
INSERT [dbo].[Book] ([Book_ID], [Book_Name], [Book_Exp], [Book_Donor], [Book_Receiving], [BCs_ID], [BSh_ID], [Aut_ID], [Pub_ID], [Lib_ID]) VALUES (N'B0001', N'Kitap2.1', NULL, NULL, CAST(N'2016-12-12' AS Date), N'BCS0001', N'BSH0001', N'AUT0001', N'PUB0001', N'L0002')
INSERT [dbo].[Book] ([Book_ID], [Book_Name], [Book_Exp], [Book_Donor], [Book_Receiving], [BCs_ID], [BSh_ID], [Aut_ID], [Pub_ID], [Lib_ID]) VALUES (N'B0002', N'Kitap2.2', NULL, NULL, CAST(N'2016-12-12' AS Date), N'BCS0001', N'BSH0001', N'AUT0001', N'PUB0001', N'L0002')
INSERT [dbo].[Book] ([Book_ID], [Book_Name], [Book_Exp], [Book_Donor], [Book_Receiving], [BCs_ID], [BSh_ID], [Aut_ID], [Pub_ID], [Lib_ID]) VALUES (N'B0003', N'Kitap2.3', NULL, NULL, CAST(N'2016-12-12' AS Date), N'BCS0001', N'BSH0001', N'AUT0001', N'PUB0001', N'L0002')
INSERT [dbo].[Bookcase] ([BCs_ID], [BCs_Loc], [BCs_Exp], [Lib_ID]) VALUES (N'BCS0001', NULL, NULL, N'K0001')
INSERT [dbo].[Bookcase] ([BCs_ID], [BCs_Loc], [BCs_Exp], [Lib_ID]) VALUES (N'BCS0001', NULL, NULL, N'L0002')
INSERT [dbo].[Bookcase] ([BCs_ID], [BCs_Loc], [BCs_Exp], [Lib_ID]) VALUES (N'BCS0002', NULL, NULL, N'K0001')
INSERT [dbo].[Bookcase] ([BCs_ID], [BCs_Loc], [BCs_Exp], [Lib_ID]) VALUES (N'BCS0002', NULL, NULL, N'L0002')
INSERT [dbo].[Bookshelf] ([BSh_ID], [BSh_Exp], [Cat_ID], [BCs_ID], [Lib_ID]) VALUES (N'BSH0001', NULL, 1, N'BCS0001', N'K0001')
INSERT [dbo].[Bookshelf] ([BSh_ID], [BSh_Exp], [Cat_ID], [BCs_ID], [Lib_ID]) VALUES (N'BSH0001', NULL, 1, N'BCS0001', N'L0002')
INSERT [dbo].[Bookshelf] ([BSh_ID], [BSh_Exp], [Cat_ID], [BCs_ID], [Lib_ID]) VALUES (N'BSH0002', NULL, 2, N'BCS0002', N'K0001')
INSERT [dbo].[Bookshelf] ([BSh_ID], [BSh_Exp], [Cat_ID], [BCs_ID], [Lib_ID]) VALUES (N'BSH0002', NULL, 2, N'BCS0002', N'L0002')
INSERT [dbo].[Category] ([Cat_ID], [Cat_Name], [Cat_Exp]) VALUES (1, N'Bilim', NULL)
INSERT [dbo].[Category] ([Cat_ID], [Cat_Name], [Cat_Exp]) VALUES (2, N'Matematik', NULL)
INSERT [dbo].[Category_Book] ([Book_ID], [Lib_ID], [Cat_ID]) VALUES (N'B00001', N'K0001', 1)
INSERT [dbo].[Category_Book] ([Book_ID], [Lib_ID], [Cat_ID]) VALUES (N'B00001', N'K0001', 2)
INSERT [dbo].[Category_Book] ([Book_ID], [Lib_ID], [Cat_ID]) VALUES (N'B00002', N'K0001', 1)
INSERT [dbo].[Category_Book] ([Book_ID], [Lib_ID], [Cat_ID]) VALUES (N'B00002', N'K0001', 2)
INSERT [dbo].[Employee] ([Emp_ID], [Emp_TCID], [Emp_Name], [Emp_SurName], [Emp_PhoneNo], [Emp_Salary], [Emp_Task], [Emp_Birth], [Emp_Entry], [Emp_Dissmissal], [Emp_Footnote], [Lib_ID]) VALUES (N'0001', N'5345674432', N'Şeyma     ', N'Gür       ', N'5053347754', 10000, N'Müdür', CAST(N'1990-02-01' AS Date), CAST(N'2001-05-02' AS Date), NULL, NULL, N'K0001')
INSERT [dbo].[Employee] ([Emp_ID], [Emp_TCID], [Emp_Name], [Emp_SurName], [Emp_PhoneNo], [Emp_Salary], [Emp_Task], [Emp_Birth], [Emp_Entry], [Emp_Dissmissal], [Emp_Footnote], [Lib_ID]) VALUES (N'0002', N'5345674432', N'Ali       ', N'Tekenci   ', N'5053347754', 14000, N'Müdür', CAST(N'1987-02-01' AS Date), CAST(N'1999-03-23' AS Date), NULL, NULL, N'L0002')
INSERT [dbo].[Employee] ([Emp_ID], [Emp_TCID], [Emp_Name], [Emp_SurName], [Emp_PhoneNo], [Emp_Salary], [Emp_Task], [Emp_Birth], [Emp_Entry], [Emp_Dissmissal], [Emp_Footnote], [Lib_ID]) VALUES (N'0003', N'5345674432', N'Ekrem     ', N'Falcı     ', N'5053347754', 5000, N'', CAST(N'1993-02-01' AS Date), CAST(N'2002-05-02' AS Date), NULL, NULL, N'K0001')
INSERT [dbo].[Employee] ([Emp_ID], [Emp_TCID], [Emp_Name], [Emp_SurName], [Emp_PhoneNo], [Emp_Salary], [Emp_Task], [Emp_Birth], [Emp_Entry], [Emp_Dissmissal], [Emp_Footnote], [Lib_ID]) VALUES (N'0004', N'5345674432', N'Ayşe      ', N'Falcı     ', N'5053347754', 5000, N'', CAST(N'1995-02-01' AS Date), CAST(N'2002-05-02' AS Date), NULL, NULL, N'K0001')
INSERT [dbo].[Employee] ([Emp_ID], [Emp_TCID], [Emp_Name], [Emp_SurName], [Emp_PhoneNo], [Emp_Salary], [Emp_Task], [Emp_Birth], [Emp_Entry], [Emp_Dissmissal], [Emp_Footnote], [Lib_ID]) VALUES (N'0005', N'5345674432', N'Ekrem     ', N'Kantakoğlu', N'5053347754', 5100, N'', CAST(N'1994-02-01' AS Date), CAST(N'2003-05-02' AS Date), NULL, NULL, N'L0002')
INSERT [dbo].[Library] ([Lib_ID], [Lib_Name], [Address_ID]) VALUES (N'K0001', N'Esenyurt Kütüphanesi', N'A0001')
INSERT [dbo].[Library] ([Lib_ID], [Lib_Name], [Address_ID]) VALUES (N'L0002', N'Lib2', N'A0002')
INSERT [dbo].[Publisher] ([Pub_ID], [Pub_Name]) VALUES (N'PUB0001', N'Asaf Yayınları')
SET ANSI_PADDING ON

GO
/****** Object:  Index [UQ_Library_Address_ID]    Script Date: 7.11.2018 22:05:27 ******/
ALTER TABLE [dbo].[Library] ADD  CONSTRAINT [UQ_Library_Address_ID] UNIQUE NONCLUSTERED 
(
	[Address_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UQ__Member__816CC0F6C19461BD]    Script Date: 7.11.2018 22:05:27 ******/
ALTER TABLE [dbo].[Member] ADD UNIQUE NONCLUSTERED 
(
	[Mem_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Book]  WITH CHECK ADD  CONSTRAINT [FK_Book_Author] FOREIGN KEY([Aut_ID])
REFERENCES [dbo].[Author] ([Aut_ID])
GO
ALTER TABLE [dbo].[Book] CHECK CONSTRAINT [FK_Book_Author]
GO
ALTER TABLE [dbo].[Book]  WITH CHECK ADD  CONSTRAINT [FK_Book_Bookcase] FOREIGN KEY([BCs_ID], [Lib_ID])
REFERENCES [dbo].[Bookcase] ([BCs_ID], [Lib_ID])
GO
ALTER TABLE [dbo].[Book] CHECK CONSTRAINT [FK_Book_Bookcase]
GO
ALTER TABLE [dbo].[Book]  WITH CHECK ADD  CONSTRAINT [FK_Book_Bookshelf] FOREIGN KEY([BSh_ID], [BCs_ID], [Lib_ID])
REFERENCES [dbo].[Bookshelf] ([BSh_ID], [BCs_ID], [Lib_ID])
GO
ALTER TABLE [dbo].[Book] CHECK CONSTRAINT [FK_Book_Bookshelf]
GO
ALTER TABLE [dbo].[Book]  WITH CHECK ADD  CONSTRAINT [FK_Book_Library] FOREIGN KEY([Lib_ID])
REFERENCES [dbo].[Library] ([Lib_ID])
GO
ALTER TABLE [dbo].[Book] CHECK CONSTRAINT [FK_Book_Library]
GO
ALTER TABLE [dbo].[Book]  WITH CHECK ADD  CONSTRAINT [FK_Book_Publisher] FOREIGN KEY([Pub_ID])
REFERENCES [dbo].[Publisher] ([Pub_ID])
GO
ALTER TABLE [dbo].[Book] CHECK CONSTRAINT [FK_Book_Publisher]
GO
ALTER TABLE [dbo].[Bookcase]  WITH CHECK ADD  CONSTRAINT [FK_Bookcase_Library] FOREIGN KEY([Lib_ID])
REFERENCES [dbo].[Library] ([Lib_ID])
GO
ALTER TABLE [dbo].[Bookcase] CHECK CONSTRAINT [FK_Bookcase_Library]
GO
ALTER TABLE [dbo].[Bookshelf]  WITH CHECK ADD  CONSTRAINT [FK_Bookshelf_Bookcase] FOREIGN KEY([BCs_ID], [Lib_ID])
REFERENCES [dbo].[Bookcase] ([BCs_ID], [Lib_ID])
GO
ALTER TABLE [dbo].[Bookshelf] CHECK CONSTRAINT [FK_Bookshelf_Bookcase]
GO
ALTER TABLE [dbo].[Bookshelf]  WITH CHECK ADD  CONSTRAINT [FK_Bookshelf_Category] FOREIGN KEY([Cat_ID])
REFERENCES [dbo].[Category] ([Cat_ID])
GO
ALTER TABLE [dbo].[Bookshelf] CHECK CONSTRAINT [FK_Bookshelf_Category]
GO
ALTER TABLE [dbo].[Bookshelf]  WITH CHECK ADD  CONSTRAINT [FK_Bookshelf_Library] FOREIGN KEY([Lib_ID])
REFERENCES [dbo].[Library] ([Lib_ID])
GO
ALTER TABLE [dbo].[Bookshelf] CHECK CONSTRAINT [FK_Bookshelf_Library]
GO
ALTER TABLE [dbo].[Category_Book]  WITH CHECK ADD  CONSTRAINT [FK_Category_Book_Book] FOREIGN KEY([Book_ID], [Lib_ID])
REFERENCES [dbo].[Book] ([Book_ID], [Lib_ID])
GO
ALTER TABLE [dbo].[Category_Book] CHECK CONSTRAINT [FK_Category_Book_Book]
GO
ALTER TABLE [dbo].[Category_Book]  WITH CHECK ADD  CONSTRAINT [FK_Category_Book_Category] FOREIGN KEY([Cat_ID])
REFERENCES [dbo].[Category] ([Cat_ID])
GO
ALTER TABLE [dbo].[Category_Book] CHECK CONSTRAINT [FK_Category_Book_Category]
GO
ALTER TABLE [dbo].[Employee]  WITH CHECK ADD  CONSTRAINT [FK_Employee_Library] FOREIGN KEY([Lib_ID])
REFERENCES [dbo].[Library] ([Lib_ID])
GO
ALTER TABLE [dbo].[Employee] CHECK CONSTRAINT [FK_Employee_Library]
GO
ALTER TABLE [dbo].[Escrow]  WITH CHECK ADD  CONSTRAINT [FK_Escrow_Book] FOREIGN KEY([Book_ID], [Lib_ID])
REFERENCES [dbo].[Book] ([Book_ID], [Lib_ID])
GO
ALTER TABLE [dbo].[Escrow] CHECK CONSTRAINT [FK_Escrow_Book]
GO
ALTER TABLE [dbo].[Escrow]  WITH CHECK ADD  CONSTRAINT [FK_Escrow_Library] FOREIGN KEY([Lib_ID])
REFERENCES [dbo].[Library] ([Lib_ID])
GO
ALTER TABLE [dbo].[Escrow] CHECK CONSTRAINT [FK_Escrow_Library]
GO
ALTER TABLE [dbo].[Escrow]  WITH CHECK ADD  CONSTRAINT [FK_Escrow_MemberID] FOREIGN KEY([Mem_ID])
REFERENCES [dbo].[Member] ([Mem_ID])
GO
ALTER TABLE [dbo].[Escrow] CHECK CONSTRAINT [FK_Escrow_MemberID]
GO
ALTER TABLE [dbo].[Escrow]  WITH CHECK ADD  CONSTRAINT [FK_Escrow_MemberTCID] FOREIGN KEY([Mem_TCID])
REFERENCES [dbo].[Member] ([Mem_TCID])
GO
ALTER TABLE [dbo].[Escrow] CHECK CONSTRAINT [FK_Escrow_MemberTCID]
GO
ALTER TABLE [dbo].[Library]  WITH CHECK ADD  CONSTRAINT [FK_Library_Address] FOREIGN KEY([Address_ID])
REFERENCES [dbo].[Address] ([Address_ID])
GO
ALTER TABLE [dbo].[Library] CHECK CONSTRAINT [FK_Library_Address]
GO
ALTER TABLE [dbo].[Member]  WITH CHECK ADD  CONSTRAINT [FK_Member_Library] FOREIGN KEY([Lib_ID])
REFERENCES [dbo].[Library] ([Lib_ID])
GO
ALTER TABLE [dbo].[Member] CHECK CONSTRAINT [FK_Member_Library]
GO
USE [master]
GO
ALTER DATABASE [LibraryDB] SET  READ_WRITE 
GO
